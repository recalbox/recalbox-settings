# recalbox-tools

## purpose

Set of simple & optimized C/C++ tools

- recalbox_settings : provide simple and efficient methods to read, write and disable keys in recalbox.conf
- recallog : fast log with string unescaping 

## compile

Use cmake or CLion.
